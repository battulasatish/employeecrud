package com.antra.emp.Exeption;

public class EmployeeDoesNotExist extends RuntimeException {
	private String msg;
	public EmployeeDoesNotExist(String msg) {
		super(msg);
		this.msg=msg;
	}

}
