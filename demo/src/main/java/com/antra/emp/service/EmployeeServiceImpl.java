package com.antra.emp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.antra.emp.entity.EmployeeEntity;
import com.antra.emp.model.EmployeeModel;
import com.antra.emp.repository.EmployeeRepository;
import com.antra.emp.utility.EmployeeUtility;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository repo;

	@Override
	public String addEmployee(EmployeeModel e) {
		boolean flag=repo.existsById(e.getEmpId());
		if(flag==false) {
			EmployeeEntity emp=EmployeeUtility.modelToEntity(e);
			repo.save(emp);
			return "Employee saved successfully";
		}
		else {
			return "Employee with given id already exists";
		}
	}

	@Override
	public String updateEmployee(EmployeeModel e) {
		boolean flag=repo.existsById(e.getEmpId());
		if(flag==true) {
			EmployeeEntity emp=EmployeeUtility.modelToEntity(e);
			repo.saveAndFlush(emp);
			return "Employee updated successfully";
		}
		else {
			return "Employee with given does not exists";
		}
	}

	@Override
	public String deleteEmployee(int id) {
		boolean flag=repo.existsById(id);
		if(flag==true) {
			repo.deleteById(id);
			return "Employee delete successfully";
		}
		else {
			return "Employee with given does not exists";
		}
	}

	@Override
	public EmployeeModel getEmployee(int i) {
		boolean flag=repo.existsById(i);
		if(flag==true) {
		EmployeeEntity emp=repo.getById(i);
		EmployeeModel model=EmployeeUtility.entityToModel(emp);
		return model;
		}
		else {
			return null;
		}
	}

}
