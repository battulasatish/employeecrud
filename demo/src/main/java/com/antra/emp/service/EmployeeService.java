package com.antra.emp.service;

import com.antra.emp.model.EmployeeModel;

public interface EmployeeService {
	String addEmployee(EmployeeModel e);
	String updateEmployee(EmployeeModel e);
	String deleteEmployee(int id);
	EmployeeModel getEmployee(int i);
}
